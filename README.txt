OVERVIEW
========

With CCK and Views, you almost don't need to write SQL queries to create and
manage tables, insert into them, and then select from them.  Views will let
you query most any field in any table, since major modules expose their tables
to Views.  Filters are what allow you to create your WHERE clause, with the
ability to expose some of the input values to user selection.  Arguments allow
users to pass in values that will also be substituted in the WHERE clause.
Unfortunately, arguments and filters don't play together and by default there's
no way to use a Views query by passing in values to be used in the filters,
even though the filters allow more expressive querying (e.g. greater than,
less than, etc.) than arguments, which require an exact match to the argument.

With this module, it's now possible to replace "sentinel" values in filters
with specially formatted arguments.  The sentinel will also be replaced anywhere
else in the query, and be replaced no matter how many times it appears.  So
you can have several filters be replaced in a single argument.

This module is similar to views_arguments_in_filters which works by replacing
placeholders like "%1" with arguments in Null argument positions.  However, 
this only works for filters where free-form text is allowed for the filter's
definition.  So this won't work for taxonomy or date-based filters.  Our module
will work in these cases.

INSTALLATION & USAGE
====================

Install the module as usual. Views is a dependency.  

There is no administration page.

To use the functionality, in your View create an argument of type "Global: Null".
This argument won't directly affect the query in any way. 

Add a filter which you want to use an argument as the value for.  For example,
add a date filter on a datetime CCK field.  Think of a special value you want to
use for the date to signify that it's a sentinel, e.g. July 20, 2019.  Preview the
query to see the resulting SQL and look for the date formatted like '2014-07-20'.  
This is your sentinel.  To change this value dynamically by passing an argument, you
will pass in "sub:2014-07-20=2011-11-19" to change the value to November 19, 2011.

The format for the substitution argument is:
sub:<sentinel1>=<replacement1>,<sentinel2>=<sentinel2>,...

The string must begin with "sub:" and you can specify multiple values to search and
replace by separating the pairs with commas.  The characters ':', '=', and ',' are
special characters and must be percent-escaped.  You will need to double escape in
most cases.  The character '/' in a value must also be escaped otherwise it will be
interpreted by Drupal as an argument separator.


